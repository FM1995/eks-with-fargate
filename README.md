# EKS with Fargate

#### Project Outline

AWS Fargate is a serverless compute engine for containers that lets you run containers without managing the underlying infrastructure. When you use EKS with Fargate, it combines the benefits of Kubernetes orchestration and AWS Fargate's serverless container management. However there are limitations when using fargate as there is no support for stateful applications and no support for DaemonSets.

## Lets get started

First we have to create an IAM role for Fargate so that fargate can create the necessary resources and then attach it to EKS

Navigating to IAM -> Roles

![Image1](https://gitlab.com/FM1995/eks-with-fargate/-/raw/main/Images/Image1.png?ref_type=heads)


Can see the role is now created

![Image2](https://gitlab.com/FM1995/eks-with-fargate/-/raw/main/Images/Image2.png?ref_type=heads)

Now that we have the role, we can proceed and create the fargate profile


How it works, lets say we create a deployment, we can specify which Pods should use Fargate when they are launched


Configuring the first part with the profile name, also to note the reason why we are using our created VPC despite the fargate resources being created in another  managed account is because the sheduled pods will have an IP address from our subnet IP range. To summarise the internal components like pods can be configured in private subnets and external components like loadbalancer can be configured in the public subnet belonging to the AWS managed account.


![Image3](https://gitlab.com/FM1995/eks-with-fargate/-/raw/main/Images/Image3.png?ref_type=heads)

In the nginx deployment file we need to add a namespace so that fargate can can detect the namespace and schedule the fargate profile

![Image4](https://gitlab.com/FM1995/eks-with-fargate/-/raw/main/Images/Image4.png?ref_type=heads)

Aswell as the profile name

![Image5](https://gitlab.com/FM1995/eks-with-fargate/-/raw/main/Images/Image5.png?ref_type=heads)

And a combination of namespace and labels is how can match our pods and therefore implementing it in our deployment file

![Image6](https://gitlab.com/FM1995/eks-with-fargate/-/raw/main/Images/Image6.png?ref_type=heads)

Now ready to create and can see it is ready

![Image7](https://gitlab.com/FM1995/eks-with-fargate/-/raw/main/Images/Image7.png?ref_type=heads)

Now lets deploy our pod through fargate that will also create the deployment

Using the nginx deployment

Lets create namespace called dev


```
kubectl create ns dev
```

Can then apply the apply command

```
kubectl apply -f nginx-deployment.yaml
```

And can see it is ready

![Image8](https://gitlab.com/FM1995/eks-with-fargate/-/raw/main/Images/Image8.png?ref_type=heads)

Can see the node configured for fargate

```
kubectl get nodes -n dev
```

![Image9](https://gitlab.com/FM1995/eks-with-fargate/-/raw/main/Images/Image9.png?ref_type=heads)

Lets make a few changes for a new deployment with 'nginx-dev' with 2 replicas

![Image10](https://gitlab.com/FM1995/eks-with-fargate/-/raw/main/Images/Image10.png?ref_type=heads)

Then doing the below

```
kubectl apply -f nginx-deployment.yaml
```

And can the pods are now active

![Image11](https://gitlab.com/FM1995/eks-with-fargate/-/raw/main/Images/Image11.png?ref_type=heads)

Now it takes a while because each pods gets its own virtual machine and are running on different fargate nodes and therefore have it’s own IP address

Checking the pods on a namespace can see the below

```
kubectl get pods -n dev -o wide
```

![Image12](https://gitlab.com/FM1995/eks-with-fargate/-/raw/main/Images/Image12.png?ref_type=heads)

And can see it ec2 instance got scaled down back to minimum size due to our autoscaling group and can see for the 3 pods deployed each of them have a fargate Virtual machine attached to it

[Link to Configure Auto Scaling for EKS Repository for desired instances](https://gitlab.com/FM1995/configure-auto-scaling-for-eks)


![Image13](https://gitlab.com/FM1995/eks-with-fargate/-/raw/main/Images/Image13.png?ref_type=heads)

Lets now clean by deleting fargate profile and node-group an then the cluster

![Image14](https://gitlab.com/FM1995/eks-with-fargate/-/raw/main/Images/Image14.png?ref_type=heads)



